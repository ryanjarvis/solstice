// local state
//
let data = null;
let selected = "";
let selected_accounts = [];

// gets the data from the server once
//
// assumption: no need for error checking since this is a constrained environment
// assumption: no need for live updates
const setup = () => {
	let request = new XMLHttpRequest();
	request.onreadystatechange = () => {
		if (request.readyState != XMLHttpRequest.DONE) {
			return;
		}

		if (request.status != 200) {
			updateStatus("Failed to initialize");
			return;
		}

		data = JSON.parse(request.responseText);
		updateStatus("Online");
		drawCustomers();
		drawAllAccounts();
	};
	request.open("GET", "/api/data.json");
	request.send();
};

// updates the status element in the UI
//
const updateStatus = (state) => {
	document.getElementById("status").innerText = state;
};

// creates and populates the customer table
//
// assumption: sorting/filtering is beyond the scope of this exercise
// assumption: formatting the rows and fields is beyond scope
const drawCustomers = () => {
	let table = document.createElement("table");

	let header = document.createElement("tr");

	for (let k of Object.keys(data.customers[0])) {
		let hcell = document.createElement("th");
		hcell.append(k);
		header.append(hcell);
	};

	table.append(header);

	for (let customer of data.customers) {
		let row = document.createElement("tr");

		for (let v of Object.values(customer)) {
			let cell = document.createElement("td");
			cell.append(v);
			row.append(cell);
		};

		row.id = `cust${customer.id}`;
		row.classList.add("customer");
		row.addEventListener(
			"click",
			(event) => {
				toggleCustomer(row.id);
			}
		);

		table.append(row);
	}

	let list = document.getElementById("customer_list");
	list.append(table);
};

// Updates the UI when the user selects a particular customer
// in the customer table
//
const toggleCustomer = (cid) => {
	if (selected === cid) {
		selected = "";
	} else {
		selected = cid;
	}

	for (let element of document.getElementsByClassName("customer")) {
		element.classList.remove("selected");
		if (element.id === selected) {
			element.classList.add("selected");
		}
	};

	if (!selected) {
		document.getElementById("filtered_account_list").classList.add("invisible");
		document.getElementById("account_list").classList.remove("invisible");
		return;
	}

	selected_accounts = data.accounts.filter(
		(datum) => {
			return `cust${datum.customer_id}` === selected;
		}
	);

	if (selected_accounts.length == 0) {
		document.getElementById("filtered_account_list").classList.add("invisible");
		document.getElementById("account_list").classList.remove("invisible");
		return;
	}

	drawFilteredAccounts();

	document.getElementById("account_list").classList.add("invisible");
	document.getElementById("filtered_account_list").classList.remove("invisible");
};

// Creates and populates the All Accounts table
//
const drawAllAccounts = () => {
	let list = document.getElementById("account_list");
	drawAccounts(list, data.accounts);
};

// Creates and populates the Filtered Accounts table
//
const drawFilteredAccounts = () => {
	let list = document.getElementById("filtered_account_list");
	drawAccounts(list, selected_accounts);
};

// Creates a table containing a set of accounts.
//
const drawAccounts = (element, collection) => {
	let table = document.createElement("table");

	let header = document.createElement("tr");

	for (let k of Object.keys(collection[0])) {
		let hcell = document.createElement("th");
		hcell.append(k);
		header.append(hcell);
	};

	table.append(header);

	for (let account of collection) {
		let row = document.createElement("tr");

		for (let v of Object.values(account)) {
			let cell = document.createElement("td");
			cell.append(v);
			row.append(cell);
		};

		table.append(row);
	}

	element.innerHTML = ""; //cheap hack
	element.append(table);
};

// Starts the app when the browser finishes loading the page
//
window.addEventListener(
	"load",
	setup
);
