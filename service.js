// config
// (constants here, but could come from elsewhere)
const PORT = 8088;

// modules
const http = require("http");
const fs = require("fs");

// runtime
const server = http.createServer(
	(req, res) => {
		console.log(`${req.method}: ${req.url}`);

		// handle
		if (req.method === "GET" && req.url === "/") {
			res.writeHead(
				302,
				{
					"Location": "/public/index.html"
				}
			);
			res.end();
		}

		if (req.method === "GET" && req.url === "/public/index.html") {
			let data = fs.readFileSync("./public/index.html");
			res.writeHead(
				200,
				{
					"Content-Type": "text/html"
				}
			);
			res.end(data, "utf-8");
		}

		if (req.method === "GET" && req.url === "/public/index.js") {
			let data = fs.readFileSync("./public/index.js");
			res.writeHead(
				200,
				{
					"Content-Type": "text/javascript"
				}
			);
			res.end(data, "utf-8");
		}

		if (req.method === "GET" && req.url === "/public/index.css") {
			let data = fs.readFileSync("./public/index.css");
			res.writeHead(
				200,
				{
					"Content-Type": "text/css"
				}
			);
			res.end(data, "utf-8");
		}

		if (req.method === "GET" && req.url === "/api/data.json") {
			let data = fs.readFileSync("./api/data.json");
			res.writeHead(
				200,
				{
					"Content-Type": "application/json"
				}
			);
			res.end(data, "utf-8");
		}

		if (req.method === "GET" && req.url === "/favicon.ico") {
			res.end("missing");
		}

		res.writeHead(404);
		res.end("Sorry, not found");
	}
);

server.listen(
	PORT,
	() => {
		console.log(`Server running and listening on port ${PORT}`);
		console.log(`Press 'Ctrl-C' to terminate.`);
	}
);
