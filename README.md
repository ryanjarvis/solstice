Solstice Full Stack Interview Exercise

# Description

Create a web app that displays customer and account information and relations.

# Modifications

The data file provided, `initial.json` wasn't a valid JSON file.
It was a text file that contained JavaScript objects so I converted into a valid JSON file.

Implementation is framework agnostic and written entirely in vanilla JS.
There are no backend frameworks (Koa, Express, Hapi, etc.)
There are no frontend frameworks (React, Angular, Vue, etc.)
There are zero dependencies to install.

# Assumptions

Server:

*  A small dataset; Reading the json data from the file system once in an entire set is fine.
*  Correctly formatted data.
*  API does not need to be considered REST.
*  Data does not need to be cached.
*  Authentication is beyond scope.
*  Data does not need to be changed in real time.

Client:

*  Error checking and validating the data from the server is beyond the scope of this exercise.
*  Does not need live data updates from the server.
*  Filtering, sorting, and formatting rows and fields in the tables are beyond scope.

Misc:

*  User Experience and design are beyond the scope of this exercise
*  Databases are beyond scope
*  Networking and Operating Systems are beyond scope
*  Algorithms, time and space efficiency are not targetted
*  Modularlity and componentization is beyond scope (typically language and framework dependent)

# Dependencies

Zero

# How to Run

Change the webserver port to what you want on line 3 of `service.js` (default: 8088);

Run `node service.js`

Navigate to `localhost:8088` in your browser

# Environment

Developed and tested on Node v13.5.0 / OSX 10.15.4 with Google Chrome 80.0.3987.163
